import Vue from "nativescript-vue";
import App from "./components/App";

// const firebase = require("@nativescript/firebase");
import { firebase } from '@nativescript/firebase';

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === "production");

firebase.init({}).then(
  () => {
    console.log("firebase successfully initialized");
  },
  error => {
    console.log(`firebase.init error: ${error}`);
  }
);

new Vue({
  render: h => h('frame', [h(App)])
}).$start();
