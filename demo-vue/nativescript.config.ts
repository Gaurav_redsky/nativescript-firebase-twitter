import { NativeScriptConfig } from '@nativescript/core'

export default {
  id: 'org.nativescript.firebasedemo.twitter',
  appResourcesPath: 'app/App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'full',
  },
  discardUncaughtJsExceptions: true,
  appPath: 'app',
} as NativeScriptConfig
